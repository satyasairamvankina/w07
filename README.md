M07 Guestbook Example
Here I am creating the simple guest book using  Node, Express, BootStrap and EJS

How to Run:

Open a command window in your Location of M07 folder.

Run npm install to install all the dependencies in the package.json file.

Run nodemon gbapp.js to start the server.Copy the link you got and paste in the browser

Commands:
> npm install
> node gbapp.js
Link :
 http://127.0.0.1:8081/ Where 127.0.0.1 is your local host address

I have changed the styling of the W07 using css and included css file and Linked it to the header.ejs file